# README #

This project holds all MarlinFW configuration for the 3D printers at BinaryThunder.org

### 3D Printers ###

* Creality Ender 5 Pro With Upgrades
    * BLTouch 3.1
    * BTT Smart Filament Sensor
    * MeanWell 450w 24v Power Supply
    * BTT GTR 1.0 + GTR Direct M5 v 1.0
    * 6x TMC2209 Stepper Drivers
    * Creality Glass Bed
    * BTT TFT70 Touch Screen
    * Octopi on raspberry pi 4B - 4GB
    * Pi Camera v2 for Octolapse
    * Micro-swiss Direct Drive Extruder
    * Micro-swiss All Metal Hot End

### Creating Custom Configurations ###

### Building Marlin ###

### PlatformIO ###
Marlin has moved to use the awesome PlatformIO for compiling the code.  To install, simply issue:

* python3 -c "$(curl -fsSL https://raw.githubusercontent.com/platformio/platformio/master/scripts/get-platformio.py)"

This will setup a virtual environment and install platoformIO into it.

### Git ###
The Marlin Firmware is included as a subtree within this project.  To set this up we followed:

* git clone git@bitbucket.org:binarythunder/marlin.git binarythunder/marlin
* cd marlin
* git remote add -f marlinfw https://github.com/MarlinFirmware/Marlin.git
* git subtree add --prefix='marlinfw' marlinfw {branch} --squash

This will put this code on the branch requested.  To update the branch to the latest use:

* git subtree pull --prefix='marlinfw' malinfw {branch} -m "Pull upstream {branch} to binarythunder/marlin"

To upgrade Marlin to a new release, use:

* git subtree pull --prefix=marlinfw marlinfw {new_branch} --squash -m "Upgrading to Marlin {new_branch}"

If you pulled the subtree for 2.x for instance, then this branch always points to the latest release adn you just need to repull it to get the latest updates. 

